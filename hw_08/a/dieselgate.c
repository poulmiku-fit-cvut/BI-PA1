#ifndef __PROGTEST__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#define LIST_BY_YEAR       0
#define LIST_BY_TYPE       1

#define TYPE_MAX           100
#define SETUP_MAX          100

typedef struct TEngine
 {
   struct TEngine * m_Next;
   int              m_Year;
   char             m_Type  [ TYPE_MAX ];
   int              m_Setup [ SETUP_MAX ];
 } TENGINE;

typedef struct TArchive
 {
   struct TArchive * m_Next;
   TENGINE         * m_Engines;
 } TARCHIVE;

TENGINE * createEngine  ( const char      * type,
                          int               year )
 {
   int i;
   TENGINE * res = (TENGINE *) malloc ( sizeof  (*res ) );

   res -> m_Next = NULL;
   res -> m_Year = year;
   strncpy ( res -> m_Type, type, sizeof ( res -> m_Type ) );
   for ( i = 0; i < SETUP_MAX; i ++ )
    res -> m_Setup[i] = 0;
   return res;
 }

void PrintEngines( TARCHIVE * archive, int listBy) {
    TENGINE * engine = archive -> m_Engines;

    while (engine != NULL) {
        if (listBy == LIST_BY_YEAR)
            printf("%d - %s\n", engine -> m_Year, engine -> m_Type);
        else
            printf("%s - %d\n", engine -> m_Type, engine -> m_Year);
        engine = engine -> m_Next;
    }
}

void PrintArchive (TARCHIVE * archive, int listBy) {
    TARCHIVE * current = archive;

    while (current != NULL) {
        PrintEngines(current, listBy);
        current = current -> m_Next;

    }
    printf("-----------------------------------\n");
}

#endif /* __PROGTEST__ */

int CompareEngines(TENGINE * first, TENGINE * second, int listBy) {
    int ret;

    if (listBy == LIST_BY_YEAR) {
        if ( first -> m_Year <= second -> m_Year) ret = 1;
        else ret = 0;
    } else {
        if ( strcmp(first -> m_Type, second -> m_Type) <= 0) ret = 1;
        else ret = 0;
    }

    return ret;
}


void AppendToArchive(TARCHIVE * archive, int listBy, TENGINE * engine) {
    if (archive -> m_Engines == NULL) {
        /*printf("Nothing in the archive...\n");*/
        archive -> m_Engines = engine;
        engine -> m_Next = NULL;
    } else {
        TENGINE * current = archive -> m_Engines, * next = current -> m_Next;
        TENGINE * prev = NULL;

        if (next == NULL) {
            if (CompareEngines(current , engine, !listBy)) {
                current -> m_Next = engine;
                engine -> m_Next = NULL;
                return;

            } else {
                archive -> m_Engines = engine;
                engine -> m_Next = current;
                current -> m_Next = NULL;
                return;
            }
        } else if ( CompareEngines( engine, current, !listBy) ) {
            /*printf("should be first %s\n", engine -> m_Type);*/
            archive -> m_Engines = engine;
            engine -> m_Next = current;
            current -> m_Next = next;
            return;
        }

        while (next != NULL) {

            if ( CompareEngines(current, engine, !listBy) && CompareEngines(engine, next, !listBy) ) {
                /*printf("Putting %s inbetween %s and %s\n", engine->m_Type, current->m_Type, next->m_Type);*/
                current -> m_Next = engine;
                engine -> m_Next = next;
                return;
            }

            prev = current;
            current = next;
            next = current -> m_Next;
        };

        /*printf("Putting %s at the end, after %s\n", engine->m_Type, current->m_Type);*/
        if ( CompareEngines(current, engine, !listBy)) {
            current -> m_Next = engine;
            engine -> m_Next = NULL;
        } else {
            prev -> m_Next = engine;
            engine -> m_Next = current;
            current -> m_Next = NULL;
        }




    }
}

int ExistsArchive(TARCHIVE * list, int listBy, TENGINE * engine) {
    if (list == NULL) {
        return 2;
    } else {
        TARCHIVE * temp_archive = list;
        while (temp_archive != NULL) {
            if (   (listBy == LIST_BY_YEAR && temp_archive -> m_Engines -> m_Year == engine -> m_Year)
                || (listBy == LIST_BY_TYPE && strcmp(temp_archive -> m_Engines -> m_Type, engine -> m_Type) == 0) ) {
                AppendToArchive(temp_archive, listBy, engine);
                return 1;
            }
            temp_archive = temp_archive -> m_Next;
        }
        return 0;

    }
}


TARCHIVE * AddEngine ( TARCHIVE * list,
                       int listBy,
                       TENGINE * engine ) {
    TARCHIVE * new_archive;
    TARCHIVE * current;
    TARCHIVE * next;
    TARCHIVE * prev = NULL;

    int exists = ExistsArchive(list, listBy, engine);

    /*printf("Adding %d %s\n", engine -> m_Year, engine -> m_Type);*/

    switch (exists) {
        case 1:
            return list;
        case 2:
            list = (TARCHIVE * ) malloc(sizeof(TARCHIVE));
            list -> m_Next = NULL;
            list -> m_Engines = NULL;
            AppendToArchive(list, listBy, engine);

            return list;
        default:
            current = list;
            next = list -> m_Next;
            new_archive = (TARCHIVE * ) malloc(sizeof(TARCHIVE));

            new_archive -> m_Engines = engine;
            AppendToArchive(new_archive, listBy, engine);


            if (next == NULL) {
                if (CompareEngines(current -> m_Engines, engine, listBy)) {
                    current -> m_Next = new_archive;
                    new_archive -> m_Next = NULL;
                    return current;
                } else {
                    new_archive -> m_Next = current;
                    current -> m_Next = NULL;
                    return new_archive;
                }
            } else if ( CompareEngines( engine, current -> m_Engines, listBy) ) {
                /*printf("should be first %s\n", engine -> m_Type);*/
                new_archive -> m_Next = current;
                return new_archive;
            }

            while (next != NULL) {

                if (  CompareEngines(current -> m_Engines, new_archive -> m_Engines, listBy)
                      && CompareEngines(new_archive -> m_Engines, next -> m_Engines, listBy) ) {

                    current -> m_Next = new_archive;
                    new_archive -> m_Next = next;
                    return list;
                }

                prev = current;
                current = next;
                next = current ->m_Next;
            }

            /*printf("Ending!\n");*/

            if ( CompareEngines(current -> m_Engines, engine, listBy)) {
                current -> m_Next = new_archive;
                new_archive -> m_Next = NULL;
            } else {
                prev -> m_Next = new_archive;
                new_archive -> m_Next = current;
                current -> m_Next = NULL;
            }

            /*printf("Wut? engine %d %s\n", engine -> m_Year, engine ->m_Type);*/

            return list;
    }
}
void DelArchive ( TARCHIVE * list ) {
    TARCHIVE * temp_archive;
    TENGINE * temp_engine;

    while (list != NULL) {
        while (list -> m_Engines != NULL) {
            temp_engine = list -> m_Engines;
            list -> m_Engines = list -> m_Engines ->m_Next;
            free(temp_engine);
        }
        temp_archive = list;
        list = list->m_Next;
        free(temp_archive);
    }
}

TARCHIVE * ReorderArchive ( TARCHIVE * list,
                            int listBy ) {

    TARCHIVE * current_archive = list, * prev_archive = NULL;
    /*PrintArchive(list, !listBy);*/

    list = NULL;

    while(current_archive != NULL) {
        TENGINE * current_engine = current_archive -> m_Engines, * next_engine = NULL, * and_another = NULL;

        next_engine = current_engine -> m_Next;
        if (next_engine != NULL) {
            and_another = next_engine -> m_Next;
        }

        while(current_engine != NULL) {

            current_engine -> m_Next = NULL;

            list = AddEngine(list, listBy, current_engine);

            current_engine = next_engine;
            next_engine = and_another;

            if (next_engine != NULL) {
                and_another = next_engine -> m_Next;
            } else {
                and_another = NULL;
            }
        }

        prev_archive = current_archive;
        current_archive = current_archive -> m_Next;
        free(prev_archive);
    }

    /*PrintArchive(list, listBy);*/

    return list;

}

#ifndef __PROGTEST__
int main ( int argc, char * argv [] ) {
    TARCHIVE * a;
    int year;
    char type[10];

    /*a = NULL;

    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "TDI 1.9", 2010 ) );

    assert ( a
                && a -> m_Engines
                && a -> m_Engines -> m_Year == 2010
                && ! strcmp ( a -> m_Engines -> m_Type, "TDI 1.9" )
                && a -> m_Engines -> m_Next == NULL
                && a -> m_Next == NULL );
    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "TDI 1.9", 2005 ) );

    assert ( a
                && a -> m_Engines
                && a -> m_Engines -> m_Year == 2005
                && ! strcmp ( a -> m_Engines -> m_Type, "TDI 1.9" )
                && a -> m_Engines -> m_Next == NULL
                && a -> m_Next
                && a -> m_Next -> m_Engines
                && a -> m_Next -> m_Engines -> m_Year == 2010
                && ! strcmp ( a -> m_Next -> m_Engines -> m_Type, "TDI 1.9" )
                && a -> m_Next -> m_Engines -> m_Next == NULL
                && a -> m_Next -> m_Next == NULL );
    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "TSI 1.2", 2010 ) );

    assert ( a
                && a -> m_Engines
                && a -> m_Engines -> m_Year == 2005
                && ! strcmp ( a -> m_Engines -> m_Type, "TDI 1.9" )
                && a -> m_Engines -> m_Next == NULL
                && a -> m_Next
                && a -> m_Next -> m_Engines
                && a -> m_Next -> m_Engines -> m_Year == 2010
                && ! strcmp ( a -> m_Next -> m_Engines -> m_Type, "TDI 1.9" )
                && a -> m_Next -> m_Engines -> m_Next -> m_Year == 2010
                && ! strcmp ( a -> m_Next -> m_Engines -> m_Next -> m_Type, "TSI 1.2" )
                && a -> m_Next -> m_Engines -> m_Next -> m_Next == NULL
                && a -> m_Next -> m_Next == NULL );
    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "TDI 2.0", 2005 ) );

    assert ( a
                && a -> m_Engines
                && a -> m_Engines -> m_Year == 2005
                && ! strcmp ( a -> m_Engines -> m_Type, "TDI 1.9" )
                && a -> m_Engines -> m_Next -> m_Year == 2005
                && ! strcmp ( a -> m_Engines -> m_Next -> m_Type, "TDI 2.0" )
                && a -> m_Engines -> m_Next -> m_Next == NULL
                && a -> m_Next
                && a -> m_Next -> m_Engines
                && a -> m_Next -> m_Engines -> m_Year == 2010
                && ! strcmp ( a -> m_Next -> m_Engines -> m_Type, "TDI 1.9" )
                && a -> m_Next -> m_Engines -> m_Next -> m_Year == 2010
                && ! strcmp ( a -> m_Next -> m_Engines -> m_Next -> m_Type, "TSI 1.2" )
                && a -> m_Next -> m_Engines -> m_Next -> m_Next == NULL
                && a -> m_Next -> m_Next == NULL );

    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "MPI 1.4", 2005 ) );

    assert ( a
                && a -> m_Engines
                && a -> m_Engines -> m_Year == 2005
                && ! strcmp ( a -> m_Engines -> m_Type, "MPI 1.4" )
                && a -> m_Engines -> m_Next -> m_Year == 2005
                && ! strcmp ( a -> m_Engines -> m_Next -> m_Type, "TDI 1.9" )
                && a -> m_Engines -> m_Next -> m_Next -> m_Year == 2005
                && ! strcmp ( a -> m_Engines -> m_Next -> m_Next -> m_Type, "TDI 2.0" )
                && a -> m_Engines -> m_Next -> m_Next -> m_Next == NULL
                && a -> m_Next
                && a -> m_Next -> m_Engines
                && a -> m_Next -> m_Engines -> m_Year == 2010
                && ! strcmp ( a -> m_Next -> m_Engines -> m_Type, "TDI 1.9" )
                && a -> m_Next -> m_Engines -> m_Next -> m_Year == 2010
                && ! strcmp ( a -> m_Next -> m_Engines -> m_Next -> m_Type, "TSI 1.2" )
                && a -> m_Next -> m_Engines -> m_Next -> m_Next == NULL
                && a -> m_Next -> m_Next == NULL );


    a = ReorderArchive ( a, LIST_BY_TYPE );

    assert ( a
                && a -> m_Engines
                && a -> m_Engines -> m_Year == 2005
                && ! strcmp ( a -> m_Engines -> m_Type, "MPI 1.4" )
                && a -> m_Engines -> m_Next == NULL
                && a -> m_Next
                && a -> m_Next -> m_Engines
                && a -> m_Next -> m_Engines -> m_Year == 2005
                && ! strcmp ( a -> m_Next -> m_Engines -> m_Type, "TDI 1.9" )
                && a -> m_Next -> m_Engines -> m_Next -> m_Year == 2010
                && ! strcmp ( a -> m_Next -> m_Engines -> m_Next -> m_Type, "TDI 1.9" )
                && a -> m_Next -> m_Engines -> m_Next -> m_Next == NULL
                && a -> m_Next -> m_Next
                && a -> m_Next -> m_Next -> m_Engines
                && a -> m_Next -> m_Next -> m_Engines -> m_Year == 2005
                && ! strcmp ( a -> m_Next -> m_Next -> m_Engines -> m_Type, "TDI 2.0" )
                && a -> m_Next -> m_Next -> m_Engines -> m_Next == NULL
                && a -> m_Next -> m_Next -> m_Next
                && a -> m_Next -> m_Next -> m_Next -> m_Engines
                && a -> m_Next -> m_Next -> m_Next -> m_Engines -> m_Year == 2010
                && ! strcmp ( a -> m_Next -> m_Next -> m_Next -> m_Engines -> m_Type, "TSI 1.2" )
                && a -> m_Next -> m_Next -> m_Next -> m_Engines -> m_Next == NULL
                && a -> m_Next -> m_Next -> m_Next -> m_Next == NULL );
    DelArchive ( a );

    a = NULL;
    a = AddEngine ( a, LIST_BY_TYPE, createEngine ( "TDI 1.9", 2010 ) );
    assert ( a
                && a -> m_Engines
                && a -> m_Engines -> m_Year == 2010
                && ! strcmp ( a -> m_Engines -> m_Type, "TDI 1.9" )
                && a -> m_Engines -> m_Next == NULL
                && a -> m_Next == NULL );
    a = AddEngine ( a, LIST_BY_TYPE, createEngine ( "TDI 1.9", 2005 ) );
    assert ( a
                && a -> m_Engines
                && a -> m_Engines -> m_Year == 2005
                && ! strcmp ( a -> m_Engines -> m_Type, "TDI 1.9" )
                && a -> m_Engines -> m_Next -> m_Year == 2010
                && ! strcmp ( a -> m_Engines -> m_Next -> m_Type, "TDI 1.9" )
                && a -> m_Engines -> m_Next -> m_Next == NULL
                && a -> m_Next == NULL );
    a = AddEngine ( a, LIST_BY_TYPE, createEngine ( "TSI 1.2", 2010 ) );
    assert ( a
                && a -> m_Engines
                && a -> m_Engines -> m_Year == 2005
                && ! strcmp ( a -> m_Engines -> m_Type, "TDI 1.9" )
                && a -> m_Engines -> m_Next -> m_Year == 2010
                && ! strcmp ( a -> m_Engines -> m_Next -> m_Type, "TDI 1.9" )
                && a -> m_Engines -> m_Next -> m_Next == NULL
                && a -> m_Next
                && a -> m_Next -> m_Engines
                && a -> m_Next -> m_Engines -> m_Year == 2010
                && ! strcmp ( a -> m_Next -> m_Engines -> m_Type, "TSI 1.2" )
                && a -> m_Next -> m_Engines -> m_Next == NULL
                && a -> m_Next -> m_Next == NULL );
    a = AddEngine ( a, LIST_BY_TYPE, createEngine ( "TDI 2.0", 2005 ) );
    assert ( a
                && a -> m_Engines
                && a -> m_Engines -> m_Year == 2005
                && ! strcmp ( a -> m_Engines -> m_Type, "TDI 1.9" )
                && a -> m_Engines -> m_Next -> m_Year == 2010
                && ! strcmp ( a -> m_Engines -> m_Next -> m_Type, "TDI 1.9" )
                && a -> m_Engines -> m_Next -> m_Next == NULL
                && a -> m_Next
                && a -> m_Next -> m_Engines
                && a -> m_Next -> m_Engines -> m_Year == 2005
                && ! strcmp ( a -> m_Next -> m_Engines -> m_Type, "TDI 2.0" )
                && a -> m_Next -> m_Engines -> m_Next == NULL
                && a -> m_Next -> m_Next
                && a -> m_Next -> m_Next -> m_Engines
                && a -> m_Next -> m_Next -> m_Engines -> m_Year == 2010
                && ! strcmp ( a -> m_Next -> m_Next -> m_Engines -> m_Type, "TSI 1.2" )
                && a -> m_Next -> m_Next -> m_Engines -> m_Next == NULL
                && a -> m_Next -> m_Next -> m_Next == NULL );
    a = AddEngine ( a, LIST_BY_TYPE, createEngine ( "MPI 1.4", 2005 ) );
    assert ( a
                && a -> m_Engines
                && a -> m_Engines -> m_Year == 2005
                && ! strcmp ( a -> m_Engines -> m_Type, "MPI 1.4" )
                && a -> m_Engines -> m_Next == NULL
                && a -> m_Next
                && a -> m_Next -> m_Engines
                && a -> m_Next -> m_Engines -> m_Year == 2005
                && ! strcmp ( a -> m_Next -> m_Engines -> m_Type, "TDI 1.9" )
                && a -> m_Next -> m_Engines -> m_Next -> m_Year == 2010
                && ! strcmp ( a -> m_Next -> m_Engines -> m_Next -> m_Type, "TDI 1.9" )
                && a -> m_Next -> m_Engines -> m_Next -> m_Next == NULL
                && a -> m_Next -> m_Next
                && a -> m_Next -> m_Next -> m_Engines
                && a -> m_Next -> m_Next -> m_Engines -> m_Year == 2005
                && ! strcmp ( a -> m_Next -> m_Next -> m_Engines -> m_Type, "TDI 2.0" )
                && a -> m_Next -> m_Next -> m_Engines -> m_Next == NULL
                && a -> m_Next -> m_Next -> m_Next
                && a -> m_Next -> m_Next -> m_Next -> m_Engines
                && a -> m_Next -> m_Next -> m_Next -> m_Engines -> m_Year == 2010
                && ! strcmp ( a -> m_Next -> m_Next -> m_Next -> m_Engines -> m_Type, "TSI 1.2" )
                && a -> m_Next -> m_Next -> m_Next -> m_Engines -> m_Next == NULL
                && a -> m_Next -> m_Next -> m_Next -> m_Next == NULL );
    a = AddEngine ( a, LIST_BY_TYPE, createEngine ( "TDI 1.9", 2010 ) );
    assert ( a
                && a -> m_Engines
                && a -> m_Engines -> m_Year == 2005
                && ! strcmp ( a -> m_Engines -> m_Type, "MPI 1.4" )
                && a -> m_Engines -> m_Next == NULL
                && a -> m_Next
                && a -> m_Next -> m_Engines
                && a -> m_Next -> m_Engines -> m_Year == 2005
                && ! strcmp ( a -> m_Next -> m_Engines -> m_Type, "TDI 1.9" )
                && a -> m_Next -> m_Engines -> m_Next -> m_Year == 2010
                && ! strcmp ( a -> m_Next -> m_Engines -> m_Next -> m_Type, "TDI 1.9" )
                && a -> m_Next -> m_Engines -> m_Next -> m_Next -> m_Year == 2010
                && ! strcmp ( a -> m_Next -> m_Engines -> m_Next -> m_Next -> m_Type, "TDI 1.9" )
                && a -> m_Next -> m_Engines -> m_Next -> m_Next -> m_Next == NULL
                && a -> m_Next -> m_Next
                && a -> m_Next -> m_Next -> m_Engines
                && a -> m_Next -> m_Next -> m_Engines -> m_Year == 2005
                && ! strcmp ( a -> m_Next -> m_Next -> m_Engines -> m_Type, "TDI 2.0" )
                && a -> m_Next -> m_Next -> m_Engines -> m_Next == NULL
                && a -> m_Next -> m_Next -> m_Next
                && a -> m_Next -> m_Next -> m_Next -> m_Engines
                && a -> m_Next -> m_Next -> m_Next -> m_Engines -> m_Year == 2010
                && ! strcmp ( a -> m_Next -> m_Next -> m_Next -> m_Engines -> m_Type, "TSI 1.2" )
                && a -> m_Next -> m_Next -> m_Next -> m_Engines -> m_Next == NULL
                && a -> m_Next -> m_Next -> m_Next -> m_Next == NULL );
    a = ReorderArchive ( a, LIST_BY_YEAR );
    assert ( a
                && a -> m_Engines
                && a -> m_Engines -> m_Year == 2005
                && ! strcmp ( a -> m_Engines -> m_Type, "MPI 1.4" )
                && a -> m_Engines -> m_Next -> m_Year == 2005
                && ! strcmp ( a -> m_Engines -> m_Next -> m_Type, "TDI 1.9" )
                && a -> m_Engines -> m_Next -> m_Next -> m_Year == 2005
                && ! strcmp ( a -> m_Engines -> m_Next -> m_Next -> m_Type, "TDI 2.0" )
                && a -> m_Engines -> m_Next -> m_Next -> m_Next == NULL
                && a -> m_Next
                && a -> m_Next -> m_Engines
                && a -> m_Next -> m_Engines -> m_Year == 2010
                && ! strcmp ( a -> m_Next -> m_Engines -> m_Type, "TDI 1.9" )
                && a -> m_Next -> m_Engines -> m_Next -> m_Year == 2010
                && ! strcmp ( a -> m_Next -> m_Engines -> m_Next -> m_Type, "TDI 1.9" )
                && a -> m_Next -> m_Engines -> m_Next -> m_Next -> m_Year == 2010
                && ! strcmp ( a -> m_Next -> m_Engines -> m_Next -> m_Next -> m_Type, "TSI 1.2" )
                && a -> m_Next -> m_Engines -> m_Next -> m_Next -> m_Next == NULL
                && a -> m_Next -> m_Next == NULL );
    DelArchive ( a );
     a = NULL;

    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "C", 2005 ) );
    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "B", 2005 ) );
    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "A", 2005 ) );
    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "C", 2005 ) );
    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "A", 2005 ) );
    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "B", 2005 ) );
    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "C", 2005 ) );
    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "A", 2005 ) );
    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "B", 2005 ) );
    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "C", 2005 ) );
    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "A", 2005 ) );
    a = AddEngine ( a, LIST_BY_YEAR, createEngine ( "B", 2005 ) );

    PrintArchive(a, LIST_BY_YEAR);
    a = ReorderArchive(a, LIST_BY_TYPE);
    PrintArchive(a, LIST_BY_TYPE);
    DelArchive(a);*/

    a = NULL;



    while(scanf("%d %s", &year, type ) != -1) {
        if (year == 0) PrintArchive(a, LIST_BY_YEAR);
        else if (year == 1) {
            a = ReorderArchive(a, LIST_BY_TYPE);
        }
        else if (year == 2) PrintArchive(a, LIST_BY_TYPE);
        else if (year == 3) DelArchive(a);
        else {
            a = AddEngine ( a, LIST_BY_YEAR, createEngine ( type, year ) );
        }
    }

    return 0;
 }
#endif /* __PROGTEST__ */
