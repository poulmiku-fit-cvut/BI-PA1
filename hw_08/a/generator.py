# encoding=utf-8
import os
import string
import operator
from random import randint, choice

YEAR_RANGE = [2000, 2020]
SAMPLE_FOLDER = "sample"
MAX_OPERATIONS = 10000

i = 0

for x in os.walk(SAMPLE_FOLDER):
    for y in x[2]:
        if y != ".gitkeep" and int(y.split("_")[0]) > i:
            i = int(y.split("_")[0])
data_id = i + 1

in_file = open(SAMPLE_FOLDER + "/%04d_in.txt" % data_id, "w")
out_file = open(SAMPLE_FOLDER + "/%04d_out.txt" % data_id, "w")

operations = randint(10, MAX_OPERATIONS)

engines = []
i = 0
while i < MAX_OPERATIONS:
    year = randint(*YEAR_RANGE)
    _type = ""
    for x in xrange(randint(1, 10)):
        _type += choice(string.ascii_uppercase)
    engines.append((year, _type))
    in_file.write("%s %s\n" % (year, _type))
    i += 1

in_file.write("0 A\n")
in_file.write("1 A\n")
in_file.write("2 A\n")
in_file.write("3 A\n")


engines.sort(key=operator.itemgetter(0, 1))

for engine in engines:
    out_file.write("%s - %s\n" % (engine[0], engine[1]))

engines.sort(key=operator.itemgetter(1, 0))

out_file.write("-----------------------------------\n")

for engine in engines:
    out_file.write("%s - %s\n" % (engine[1], engine[0]))

out_file.write("-----------------------------------\n")
