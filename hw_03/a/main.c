#ifndef __PROGTEST__
#include <stdio.h>
#include <assert.h>
#include <limits.h>
#endif /* __PROGTEST__ */

int day_of_the_week(long long int year, int month, int day);
void next_13th(long long int * year, int * month);
int get_month_length( long long int year, int month );
int is_year_leap( long long int year );
int validate_date( long long int year, int month, int day);
int valid_span( long long int y1, int m1, int d1,
                long long int y2, int m2, int d2);
int CountFriday13 ( int y1, int m1, int d1,
                    int y2, int m2, int d2, long long int * cnt );
void initiate_year_stats(void);
int get_year(long long int * year);
long long int get_whole_years(long long int * year, long long int year2);

int year_stats[50];

void initiate_year_stats(void) {
    /*fridays the 13th in a given year that is not leap and starts with a day*/
    year_stats[1] = 2;
    year_stats[2] = 2;
    year_stats[3] = 1;
    year_stats[4] = 3;
    year_stats[5] = 1;
    year_stats[6] = 1;
    year_stats[7] = 2;

    /* fridays the 13th in a given year that is leap and starts with a day */
    year_stats[11] = 2;
    year_stats[12] = 1;
    year_stats[13] = 2;
    year_stats[14] = 2;
    year_stats[15] = 1;
    year_stats[16] = 1;
    year_stats[17] = 3;

    /* fridays in years *01 - *99 that start with a day */
    year_stats[21] = 171;
    year_stats[22] = 170;
    year_stats[23] = 168;
    year_stats[24] = 171;
    year_stats[25] = 168;
    year_stats[26] = 171;
    year_stats[27] = 169;

    /* fridays in 400 years */
    year_stats[31] = 6839;
    year_stats[32] = 6870;
    year_stats[33] = 6850;
    year_stats[34] = 6851;
    year_stats[35] = 6869;
    year_stats[36] = 6840;
    year_stats[37] = 6881;

    /* fridays in 4000 years */
    year_stats[41] = 687;
    year_stats[42] = 681;
    year_stats[43] = 685;
    year_stats[44] = 684;
    year_stats[45] = 683;
    year_stats[46] = 685;
    year_stats[47] = 683;

}

int day_of_the_week(long long int year, int month, int day) {
    long long int temp;
    temp = (day+=month<3?year--:year-2,23*month/9+day+4+year/4-year/100+year/400-year/4000)%7;

    if (temp == 0) return 7;
    else if (temp > 7) return (int)(temp-7);
    else return (int)(temp);

}

void next_13th(long long int * year, int * month) {
    if (*month + 1 > 12) {
        *month = 1;
        *year += 1;
    } else *month += 1;
}

int get_month_length( long long int year, int month ) {
    int rtrn = 0;
    switch(month) {
        case 1:
            rtrn = 31;
            break;
        case 2:
            if (is_year_leap(year)) {
                rtrn = 29;
            } else {
                rtrn = 28;
            }
            break;
        case 3:
            rtrn = 31;
            break;
        case 4:
            rtrn = 30;
            break;
        case 5:
            rtrn = 31;
            break;
        case 6:
            rtrn = 30;
            break;
        case 7:
            rtrn = 31;
            break;
        case 8:
            rtrn = 31;
            break;
        case 9:
            rtrn = 30;
            break;
        case 10:
            rtrn = 31;
            break;
        case 11:
            rtrn = 30;
            break;
        case 12:
            rtrn = 31;
            break;
    }
    return rtrn;
}

int is_year_leap( long long int year ) {
    if (year % 4000 == 0) return 0;
    else if (year % 400 == 0) return 1;
    else if (year % 100 == 0) return 0;
    else if (year % 4 == 0) return 1;
    else return 0;
}

int validate_date( long long int year, int month, int day) {
    if (year < 1900) return 0;
    else if (month < 1 || month > 12) return 0;
    else if (day < 1) return 0;
    else if (get_month_length(year, month) < day) return 0;
    else return 1;
}

int valid_span( long long int y1, int m1, int d1,
                long long int y2, int m2, int d2) {
    if (y1 > y2) return 0;
    else if (y2 > y1) return 1;
    else {
        if (m1 > m2) return 0;
        else if (m2 > m1) return 1;
        else {
            if (d1 > d2) return 0;
            else return 1;
        }
    }

}

int get_year ( long long int * year) {
    int weekday, yearId, counter = 0;
    weekday = day_of_the_week(*year, 1, 1);

    if (is_year_leap(*year)) {
        yearId = weekday + 10;
    } else {
        yearId = weekday;
    }

    counter = year_stats[yearId];

    ++*year;

    return counter;
}

long long int get_whole_years(long long int * year, long long int year2) {
    long long int counter = 0;
    /*int temp, weekday;
    long long int cent_counter = 0;*/
    do {

        /*printf("get_whole_years: %d - %lld\n", *year, counter);*/

        if (*year%4000 == 0 && *year+4000 < year2) {
            /*weekday = day_of_the_week(*year, 1, 1);
            temp = *year+4000;
            cent_counter = 0;
            do {
                cent_counter += get_year(year);
            } while (*year < temp);

            if (year_stats[30+weekday] != cent_counter) {
                printf("It doesn't match: %d, %d vs %lld\n", 30+weekday, year_stats[30+weekday], cent_counter);
            }
            counter += cent_counter;*/

            counter += year_stats[30+day_of_the_week(*year, 1, 1)];
            *year += 4000;
        } else
        if (*year%400 == 1 && *year + 400 < year2) {
            /*weekday = day_of_the_week(*year, 1, 1);
            temp = *year+399;
            cent_counter = 0;
            do {
                cent_counter += get_year(year);
            } while (*year < temp);

            if (year_stats[40+weekday] != cent_counter) {
                printf("It doesn't match: %d, %d vs %lld\n", 40+weekday, year_stats[40+weekday], cent_counter);
            }
            counter += cent_counter;*/

            counter += year_stats[40+day_of_the_week(*year, 1, 1)];
            *year += 399;
        } else if (*year%100 == 1 && *year+100 < year2) {
            /*weekday = day_of_the_week(*year, 1, 1);
            temp = *year+99;
            cent_counter = 0;
            do {
                cent_counter += get_year(year);
            } while (*year < temp);

            if (year_stats[20+weekday] != cent_counter) {
                printf("It doesn't match: %d, %d vs %lld\n", 20+weekday, year_stats[20+weekday], cent_counter);
            }
            counter += cent_counter;*/

            counter += year_stats[20+day_of_the_week(*year, 1, 1)];
            *year += 99;
        } else counter += get_year(year);

    } while ( *year < year2);

    return counter;
}

int CountFriday13 ( int y1, int m1, int d1,
                    int y2, int m2, int d2, long long int * cnt ) {
    long long int counter = 0;
    long long int year1 = (long long int)y1, year2=(long long int)y2;

    if (!(validate_date(year1, m1, d1) && validate_date(y2, m2, d2))) return 0;
    else if (!valid_span(year1, m1, d1, year2, m2, d2)) return 0;

    initiate_year_stats();

    if (d1 < 13) d1 = 13;
    else if (d1 > 13) next_13th(&year1, &m1);

    if (!valid_span(year1, m1, 13, year2, m2, d2)) {
        *cnt = counter;
        return 1;
    }

    do {

        if (m1 == 1 && year1 < year2) counter += get_whole_years(&year1, year2);
        else {
            if (day_of_the_week(year1, m1, 13) == 5) {
                ++counter;
            }

            next_13th(&year1, &m1);
        }

        /*printf("%d %d : counter = %lld\n", year1, m1, counter);*/


    } while(valid_span(year1, m1, 13, year2, m2, d2));

    *cnt = counter;
    return 1;
}

#ifndef __PROGTEST__
int main ( int argc, char * argv [] )
{
    long long int cnt;

    assert ( CountFriday13 ( 1900,  1,  1, 2015,  1,  1, &cnt ) == 1 && cnt == 197LL );
    assert ( CountFriday13 ( 1900,  1,  1, 2015,  2,  1, &cnt ) == 1 && cnt == 197LL );
    assert ( CountFriday13 ( 1900,  1,  1, 2015,  1, 13, &cnt ) == 1 && cnt == 197LL );
    assert ( CountFriday13 ( 1900,  1,  1, 2015,  2, 13, &cnt ) == 1 && cnt == 198LL );
    assert ( CountFriday13 ( 1904,  1,  1, 2015,  1,  1, &cnt ) == 1 && cnt == 189LL );
    assert ( CountFriday13 ( 1904,  1,  1, 2015,  2,  1, &cnt ) == 1 && cnt == 189LL );
    assert ( CountFriday13 ( 1904,  1,  1, 2015,  1, 13, &cnt ) == 1 && cnt == 189LL );
    assert ( CountFriday13 ( 1904,  1,  1, 2015,  2, 13, &cnt ) == 1 && cnt == 190LL );
    assert ( CountFriday13 ( 1905,  2, 13, 2015,  1,  1, &cnt ) == 1 && cnt == 187LL );
    assert ( CountFriday13 ( 1905,  2, 13, 2015,  2,  1, &cnt ) == 1 && cnt == 187LL );
    assert ( CountFriday13 ( 1905,  2, 13, 2015,  1, 13, &cnt ) == 1 && cnt == 187LL );
    assert ( CountFriday13 ( 1905,  2, 13, 2015,  2, 13, &cnt ) == 1 && cnt == 188LL );
    assert ( CountFriday13 ( 1905,  1, 13, 2015,  1,  1, &cnt ) == 1 && cnt == 188LL );
    assert ( CountFriday13 ( 1905,  1, 13, 2015,  2,  1, &cnt ) == 1 && cnt == 188LL );
    assert ( CountFriday13 ( 1905,  1, 13, 2015,  1, 13, &cnt ) == 1 && cnt == 188LL );
    assert ( CountFriday13 ( 1905,  1, 13, 2015,  2, 13, &cnt ) == 1 && cnt == 189LL );
    assert ( CountFriday13 ( 2015, 11,  1, 2015, 10,  1, &cnt ) == 0 );
    assert ( CountFriday13 ( 2015, 10, 32, 2015, 11, 10, &cnt ) == 0 );
    assert ( CountFriday13 ( 2090,  2, 29, 2090,  2, 29, &cnt ) == 0 );
    assert ( CountFriday13 ( 2096,  2, 29, 2096,  2, 29, &cnt ) == 1 && cnt == 0LL );
    assert ( CountFriday13 ( 2100,  2, 29, 2100,  2, 29, &cnt ) == 0 );
    assert ( CountFriday13 ( 2000,  2, 29, 2000,  2, 29, &cnt ) == 1 && cnt == 0LL );


    return 0;
}
#endif /* __PROGTEST__ */