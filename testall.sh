#!/bin/bash

DIR=$2
PROG=$1
FILES=$DIR"*_in.txt"
START=`date +%s.%N`

for IN_FILE in $FILES; do
  REF_FILE=`echo -n $IN_FILE | sed -e 's/in/out/g'`
  DIFF_FILE=`echo -n $IN_FILE | sed -e 's/in/diff/g'`

  TIME=`/usr/bin/time -f%E "$PROG" < "$IN_FILE" > test-result.txt`
  TOTAL=$((TOTAL+TIME));
  if [[ -f $REF_FILE ]]; then
    diff $REF_FILE test-result.txt > $DIFF_FILE

    if [[ `wc -l $DIFF_FILE | cut -d" " -f1` -gt 0  ]]; then
      echo -n "Fail: $IN_FILE - $TIME"
    else
      echo -n "Ok: $IN_FILE - $TIME"
    fi
  else
    echo -n "No reference: $IN_FILE - $TIME"
  fi
done

END=`date +%s.%N`
TM=`bc <<< $END-$START`

echo ""
echo "Total time: $TM"
