#include <stdio.h>
#include <float.h>

int get_vars(short int num, double * first, double * second) {
    if (!num) {
        printf("Hmotnost a koncentrace vysledku:\n");
    } else {
        printf("Hmotnost a koncentrace #%d:\n", num);
    }

    if (scanf("%lf %lf", first, second) != 2 ||
        *first < 0 ||
        *second < 0 ||
        *second > 1) {
        printf("Nespravny vstup.\n");
        return 0;
    } else {
        return 1;
    }

}

int main() {
    double v1, c1, v2, c2, vv, cv, r1, r2;

    if (! get_vars(1, &v1, &c1)) return 0;
    if (! get_vars(2, &v2, &c2)) return 0;
    if (! get_vars(0, &vv, &cv)) return 0;

    if (v1 + v2 < vv) {
        printf("Nelze dosahnout.\n");
        return 0;
    }

    if (c1 - c2 == 0) {
        if (v1 > vv) {
            r1 = vv;
            r2 = 0;
        } else if (v2 > vv) {
            r2 = vv;
            r1 = 0;
        } else {
            if (v1 > v2) {
                r2 = v2;
                r1 = vv - r2;
            } else {
                r1 = v1;
                r2 = vv - r1;
            }
        }
    } else {
        r2 = (cv*vv-c1*vv)/(c2-c1);
        r1 = vv - r2;
    }

    if (r1 - v1 > 10 * FLT_EPSILON * v1 || r2 - v2 > 10 * FLT_EPSILON * v2 || r1 < 0 || r2 < 0) {
        printf("Nelze dosahnout.\n");
        return 0;
    }

    printf("%.6f x #1 + %.6f x #2\n", r1, r2);

    return 0;
}