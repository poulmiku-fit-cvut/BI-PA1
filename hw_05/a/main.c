#include <stdio.h>
#include <stdlib.h>

typedef struct  {
    int count;
    int allocated;
    int * heap;
} STOCK;

STOCK * stocks = NULL;
int max_id = 0;

void free_stocks ( void ) {
    int i;
    for (i = 0; i <= max_id; ++i) {
        free(stocks[i].heap);
    }
    free(stocks);
}

void print_wrong_input( void ) {
    printf("Nespravny vstup.\n");
}

void add_offer(int id, int price) {
    int i, temp;

    ++(stocks[id].count);

    /*printf("Adding id %d price %d, count %d, allocated %d\n", id, price, stocks[id].count, stocks[id].allocated);*/

    if (stocks[id].count > stocks[id].allocated) {
        if (stocks[id].heap == NULL) {
            stocks[id].allocated = 100;
            stocks[id].heap = (int *) realloc(stocks[id].heap, stocks[id].allocated * sizeof(int));
        } else {
            stocks[id].allocated = stocks[id].count * 4;
            /*printf("Allocating new size for id %d, size %d\n", id, stocks[id].allocated);*/
            stocks[id].heap = (int *) realloc(stocks[id].heap, stocks[id].allocated * sizeof(int));
        }


    }

    i = stocks[id].count-1;
    stocks[id].heap[i] = price;

    while(stocks[id].heap[(i-1)/2] > price) {
        temp = stocks[id].heap[i];
        stocks[id].heap[i] = stocks[id].heap[(i-1)/2];
        stocks[id].heap[(i-1)/2] = temp;
        i = (i-1)/2;
    }

}

void check_node(int id, int index) {
    int i1, i2, temp, lowest;

    i1 = 2*index+1;
    i2 = 2*index+2;
    lowest = index;


    if (i1 < stocks[id].count && stocks[id].heap[lowest] > stocks[id].heap[i1]) {
        lowest = i1;

    }
    if (i2 < stocks[id].count && stocks[id].heap[lowest] > stocks[id].heap[i2]) {
        lowest = i2;

    }

    if (lowest != index) {
        temp = stocks[id].heap[index];
        stocks[id].heap[index] = stocks[id].heap[lowest];
        stocks[id].heap[lowest] = temp;
        check_node(id, lowest);
    }
}


void make_sale(int id, int price) {


    if (stocks[id].count == 0) {
        printf("Neni k dispozici.\n");
        return;
    }

    if (stocks[id].heap[0] > price) {
        printf("Neni k dispozici.\n");
        return;
    }


    printf("Prodano za %d\n", stocks[id].heap[0]);

    stocks[id].heap[0] = stocks[id].heap[--(stocks[id].count)];

    if (stocks[id].count == 0) return;

    check_node(id, 0);

    return;
}

void print_node (int id, int index, int count) {
    int i;
    if (index < stocks[id].count) {
        for (i = 0; i < count; ++i) {
            printf(" ");
        }
        printf("%d - \n", stocks[id].heap[index]);
        print_node(id, 2*index+1, count + 2);
        print_node(id, 2*index+2, count + 2);
    }
}

void print_stock (int id) {
    printf("%d: \n", id);
    if (stocks[id].heap != NULL) {
        print_node(id, 0, 2);
    }
    printf("\n");
}

void print_stocks( void ) {
    int i;
    printf("-----------------\n");
    for (i = 0; i <= max_id; ++i) {
        print_stock(i);
    }
    printf("-----------------\n");
}

int get_line( void ) {
    char type;
    int id, price, res;

    res = scanf(" %c %d %d", &type, &id, &price);
    if (res == -1) { return 0;}
    if (res != 3 || id < 0 || id > max_id || price <= 0 || !(type == '+' || type == '-') ) {
        print_wrong_input();
        return 0;
    }

    if (type == '+') add_offer(id, price);
    else make_sale(id, price);

    return 1;

}

int main(void) {

    printf("Nejvyssi ID:\n");
    if (scanf("%d", &max_id) != 1 || max_id < 1 || getchar() != '\n') {
        print_wrong_input();
        return 0;
    }

    printf("Nabidka a poptavka:\n");

    stocks = (STOCK *) malloc( ( max_id+1 ) * sizeof(STOCK) );

    while(get_line());

    free_stocks();

    return 0;
}