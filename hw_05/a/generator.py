""" A generator of sample data.
"""
# encoding=utf-8

import os
from random import randint

MAX_ID = 300000
MAX_OPERATIONS = 1000000
MAX_PRICE = 30000
SAMPLE_FOLDER = "sample"

i = 0
for x in os.walk(SAMPLE_FOLDER):
    for y in x[2]:
        if y != ".gitkeep" and int(y.split("_")[0]) > i:
            i = int(y.split("_")[0])
data_id = i + 1

in_file = open(SAMPLE_FOLDER + "/%04d_in.txt" % data_id, "w")
out_file = open(SAMPLE_FOLDER + "/%04d_out.txt" % data_id, "w")

out_file.write("Nejvyssi ID:\n")
out_file.write("Nabidka a poptavka:\n")

max_id = randint(1, MAX_ID)
in_file.write("%s\n" % max_id)
operations = randint(1, MAX_OPERATIONS)

offers = {}
i = 0
while i < operations:
    op = randint(1, 2)
    stock_id = randint(0, max_id)
    price = randint(1, MAX_PRICE)
    if op == 1:
        if stock_id not in offers:
            offers[stock_id] = []
        offers[stock_id].append(price)
        in_file.write("+ %d %d\n" % (stock_id, price))
    else:
        if stock_id not in offers:
            out_file.write("Neni k dispozici.\n")
        else:
            lowest = 0
            lowest_index = -1
            j = 0
            for pr in offers[stock_id]:
                if pr <= price:
                    if lowest_index == -1:
                        lowest_index = j
                        lowest = pr
                    if pr <= lowest:
                        lowest_index = j
                        lowest = pr
                j += 1

            if lowest_index == -1:
                out_file.write("Neni k dispozici.\n")
            else:
                offers[stock_id].pop(lowest_index)
                out_file.write("Prodano za %d\n" % lowest)
        in_file.write("- %d %d\n" % (stock_id, price))
    #  print offers

    i += 1
